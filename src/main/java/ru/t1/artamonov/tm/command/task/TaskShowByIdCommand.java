package ru.t1.artamonov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.artamonov.tm.model.Task;
import ru.t1.artamonov.tm.util.TerminalUtil;

public final class TaskShowByIdCommand extends AbstractTaskCommand {

    @NotNull
    private static final String NAME = "task-show-by-id";

    @NotNull
    private static final String DESCRIPTION = "Display task by id.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[SHOW TASK BY ID]");
        System.out.print("ENTER ID: ");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final String userId = getUserId();
        @NotNull final Task task = getTaskService().findOneById(userId, id);
        showTask(task);
    }

}
