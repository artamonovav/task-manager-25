package ru.t1.artamonov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.artamonov.tm.enumerated.Role;
import ru.t1.artamonov.tm.model.User;

public interface IAuthService {

    void checkRoles(@Nullable Role[] roles);

    @NotNull
    String getUserId();

    @NotNull
    User getUser();

    boolean isAuth();

    void login(@Nullable String login, @Nullable String password);

    void logout();

    @NotNull
    User registry(@Nullable String login, @Nullable String password, @Nullable String email);

}
